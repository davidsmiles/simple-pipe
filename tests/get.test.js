const request = require('supertest')
const app = require('../app')


describe('GET /', () => {
    
    /**
     * testing /GET endpoints
     */

    it("should be the expected output", async () => {
        const response = await request(app).get('/')

        expect(response.body.message).toBe('Hello Pipeline')
    })
})
